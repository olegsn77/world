<?php
use yii\db\Schema;
use yii\db\Migration;

class m160414_201558_Site extends Migration
{
    public function safeUp()
    {
	    $this->createTable('Site', [
            'id' => $this->primaryKey(),
            'title' => $this->string(64)->notNull(),
            'domain' => $this->string(64),
            'alias'=> $this->string(64),
            'unpublished_page' => $this->text(),
            'description' => $this->text(),
            'url' => $this->string(64),
            'keywords' => $this->string(255),
            'layout' => $this->string(64),
            'content' => $this->text(),
            'published' => $this->boolean(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),           
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('Site');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
