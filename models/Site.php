<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "Site".
 *
 * @property \MongoId|string $_id
 * @property mixed $title
 * @property mixed $domain
 * @property mixed $alias
 * @property mixed $unpublished_page
 * @property mixed $description
 * @property mixed $url
 * @property mixed $keywords
 * @property mixed $layout
 * @property mixed $content
 * @property mixed $published
 * @property mixed $created_at
 * @property mixed $updated_at
 */
class Site extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'Site';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'title',
            'domain',
            'alias',
            'unpublished_page',
            'description',
            'url',
            'keywords',
            'layout',
            'content',
            'published',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'domain', 'alias', 'unpublished_page', 'description', 'url', 'keywords', 'layout', 'content', 'published', 'created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'domain' => Yii::t('app', 'Domain'),
            'alias' => Yii::t('app', 'Alias'),
            'unpublished_page' => Yii::t('app', 'Unpublished Page'),
            'description' => Yii::t('app', 'Description'),
            'url' => Yii::t('app', 'Url'),
            'keywords' => Yii::t('app', 'Keywords'),
            'layout' => Yii::t('app', 'Layout'),
            'content' => Yii::t('app', 'Content'),
            'published' => Yii::t('app', 'Published'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
