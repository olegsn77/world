Yii 2 WORLD STATS TEST TASK
============================

SQL QUERY:

SELECT 
	Continent, 
    Region, 
    Count(LocalName) as Countries, 
    ROUND(AVG(LifeExpectancy),2) as LifeDuration, 
    SUM(Population) as Population, 
    SUM(Cities) as Cities,
    SUM(Languages) as Languages 
FROM `Country` country 
LEFT JOIN ( 
    SELECT 
        SUM(Population) as CountryPopulation, 
        Count(Name) as Cities, 
        CountryCode 
    FROM `City` 
	GROUP BY CountryCode 
) city ON (country.Code=city.CountryCode) 
LEFT JOIN ( 
    SELECT COUNT(Language) as Languages, 
    	CountryCode 
    FROM `CountryLanguage` 
    GROUP BY CountryCode 
) as lang ON (country.Code = lang.CountryCode) 
GROUP BY Region

============================

INFO:

This application is based on basic template without any external plugins and extensions. 
How it work? 
There are Base Model called World in world module. There are rules and objects for table fields. All queries, that form result table are located in WorldSearch model. All operations are made by queries, provided by Framework. Sort is made by Yii\base\Sort. Search fields builded from HAVING query. Controller for render view is default controller. Ajax provided by Yii2. Also file web/css/styles.css contains CSS for table. 
Other controllers, views and models are created by Gii. You can do CRUD Country, City and CountryLanguage tables using top menu buttons for navigate between tables. 

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      mail/               contains view files for e-mails
      messages/			  contains translations
      modules/			  contains module world with all models, controllers and views 
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

Just clone repo to webfolder. 
~~~
ssh: git clone git@bitbucket.org:olegsn77/world.git
~~~
~~~
https: git clone https://olegsn77@bitbucket.org/olegsn77/world.git
~~~
Then run 
~~~
composer update
~~~
Or you can install basic Yii and change folders from archieve.

------------

### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~


### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.

