<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "Site".
 *
 * @property integer $id
 * @property string $title
 * @property string $domain
 * @property string $alias
 * @property string $unpublished_page
 * @property string $description
 * @property string $url
 * @property string $keywords
 * @property string $layout
 * @property string $content
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 */
class Site extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'created_at', 'updated_at'], 'required'],
            [['unpublished_page', 'description', 'content'], 'string'],
            [['published', 'created_at', 'updated_at'], 'integer'],
            [['title', 'domain', 'alias', 'url', 'layout'], 'string', 'max' => 64],
            [['keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'domain' => Yii::t('app', 'Domain'),
            'alias' => Yii::t('app', 'Alias'),
            'unpublished_page' => Yii::t('app', 'Unpublished Page'),
            'description' => Yii::t('app', 'Description'),
            'url' => Yii::t('app', 'Url'),
            'keywords' => Yii::t('app', 'Keywords'),
            'layout' => Yii::t('app', 'Layout'),
            'content' => Yii::t('app', 'Content'),
            'published' => Yii::t('app', 'Published'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
