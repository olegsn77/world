<?php
	use yii\helpers\Html;
?>
<div class="site-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <pre>
	    <?php echo "\$this->context->action->uniqueId: "; print_r($this->context->action->uniqueId)?>
    </pre>
    <pre>
	    <?php echo "\$this->context->action->id: "; print_r($this->context->action->id)?>
    </pre>
    <pre>
	    <?php echo "get_class(\$this->context): "; print_r(get_class($this->context))?>
    </pre>
    <pre>
	    <?php echo "\$this->context->module->id: "; print_r($this->context->module->id)?>
    </pre>
    <p>
        <?= Html::a(Yii::t('app', 'Return Back'), ['index'], ['class' => 'btn btn-primary']) ?>
    </p>
   
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div>
