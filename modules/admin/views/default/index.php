<?php
	use yii\helpers\Html;
	use yii\helpers\Inflector;
	use yii\helpers\StringHelper;
?>
<div class="site-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <pre>
	    <?php echo "\$this->context->action->uniqueId: "; print_r($this->context->action->uniqueId)?>
    </pre>
    <pre>
	    <?php echo "\$this->context->action->id: "; print_r($this->context->action->id)?>
    </pre>
    <pre>
	    <?php echo "get_class(\$this->context): "; print_r(get_class($this->context))?>
    </pre>
    <pre>
	    <?php echo "\$this->context->module->id: "; print_r($this->context->module->id)?>
    </pre>
    <p>
        <?= Html::a(Yii::t('app', 'Create Site'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        Algorythm site creation
    </p>
    <ol>
	    <li>Push the button</li>
	    
	    <li>check migration directory</li>
	    <li>check model <?php $name = 'Site'?></li>
	    <pre>
	    <?php echo Yii::getAlias('@app/modules/'.$this->context->module->id.'/models/'.$name.'.php')?>
	    </pre>
	    <li>check controller</li>
	    <pre>
	    <?= Yii::getAlias('@' . str_replace('\\', '/', get_class($this->context))) . '.php';?>
	    </pre>
	    <li>render view</li>
	    <pre>
	    <?php
		$name = StringHelper::basename(get_class($this->context));
        echo Inflector::camel2id(substr($name, 0, strlen($name) - 10));
		?>
	    </pre>
	    <pre>
</pre>
    </ol>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div>
