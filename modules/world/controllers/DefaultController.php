<?php

namespace app\modules\world\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Sort;

use app\modules\world\models\WorldSearch;

/**
 * Default controller for the `world` module
 */
class DefaultController extends Controller
{
	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new WorldSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);


    }
}
