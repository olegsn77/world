<?php

namespace app\modules\world\models;

use Yii;

use yii\base\Model;
/**
 * This is the model class for table "Country".
 *
 * @property string $continent
 * @property string $region
 * @property integer $countries
 * @property double $lifeDuration
 * @property integer $population
 * @property integer $cities
 * @property integer $languages
*/
class World extends Model
{
	public $Continent;
	public $Region;
	public $Countries;
	public $LifeDuration;
	public $Population;
	public $Cities;
	public $Languages;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Continent', 'Region'], 'string'],
            [['LifeDuration'], 'number'],
            [['Countries','Population', 'Cities', 'Languages'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Continent' => Yii::t('app', 'Continent'),
            'Region' => Yii::t('app', 'Region'),
            'Countries' => Yii::t('app', 'Countries'),
            'LifeDuration' => Yii::t('app', 'LifeDuration'),
            'Population' => Yii::t('app', 'Population'),
            'Cities' => Yii::t('app', 'Cities'),
            'Languages' => Yii::t('app', 'Languages'),
        ];
    }
}
