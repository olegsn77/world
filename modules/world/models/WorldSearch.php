<?php

namespace app\modules\world\models;

use Yii;
use yii\db\Query;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\world\models\World;

/**
 * WorldSearch represents the model behind the search form about `app\modules\world\models\World`.
 */
class WorldSearch extends World
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Continent', 'Region', 'Countries', 'LifeDuration', 'Population', 'Cities', 'Languages'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    
    //$query = World::find();
    
    $citySubQuery = (new Query())
		->select([
			'SUM(Population) as CountryPopulation',
			'Count(Name) as Cities',
			'CountryCode'
		])
		->from('City')
		->groupBy('CountryCode');
	
	$languageSubQuery = (new Query())
		->select([
			'COUNT(Language) as Languages',
			'CountryCode'
		])
		->from('CountryLanguage')
		->groupBy('CountryCode');

	$query = (new Query())
		->select([
			'Continent', 
			'Region', 
			'Count(LocalName) as Countries', 
			'ROUND(AVG(LifeExpectancy),2) as LifeDuration', 
			'SUM(Population) as Population',
			'SUM(Cities) as Cities',
			'SUM(Languages) as Languages' 
		])
		->from('Country country')
		->leftJoin(['city' => $citySubQuery], 'country.Code = city.CountryCode')
		->leftJoin(['lang' => $languageSubQuery], 'country.Code = lang.CountryCode')
		->groupBy('Region');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
		        'pageSize' => 20,
		    ],   
		    'sort' => [
		        'attributes' => ['Continent', 'Region', 'Countries', 'LifeDuration', 'Population','Cities','Languages'],
		        'defaultOrder' => ['Continent' => SORT_ASC]
		    ],     
		]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'Continent', $this->Continent]);
        $query->andFilterWhere(['like', 'Region', $this->Region]);   
        
        if (!empty($this->Countries))
			$query->having(['=','Count(LocalName)', $this->Countries]); 
		
		if (!empty($this->LifeDuration))
			$query->having(['=','ROUND(AVG(LifeExpectancy),2)', $this->LifeDuration]); 
			
		if (!empty($this->Population))
			$query->having(['=','SUM(Population)', $this->Population]);  
			
		if (!empty($this->Cities))
			$query->having(['=','SUM(Cities)', $this->Cities]);
			
		if (!empty($this->Languages))
			$query->having(['=','SUM(Languages)', $this->Languages]);    

        return $dataProvider;
    }
}
